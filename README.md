# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://bertoncelj1@bitbucket.org/bertoncelj1/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/bertoncelj1/stroboskop/commits/6a061a32e83d5d2259b4e0cd3c09fccb72208624

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/bertoncelj1/stroboskop/commits/99b527f14cdb7fccff865022f7bd406fa25e17f4

Naloga 6.3.2:
https://bitbucket.org/bertoncelj1/stroboskop/commits/99b527f14cdb7fccff865022f7bd406fa25e17f4

Naloga 6.3.3:
https://bitbucket.org/bertoncelj1/stroboskop/commits/2f735ba21ed8a6b5939e8ef9c33a54edf4f96cc0

Naloga 6.3.4:
https://bitbucket.org/bertoncelj1/stroboskop/commits/95af0c0ff397f6aef5b1202e2a48fa9c21541651

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/bertoncelj1/stroboskop/commits/b45af1a6edd257915893747bb62a26355ce7e7e7

Naloga 6.4.2:
https://bitbucket.org/bertoncelj1/stroboskop/commits/ac3a70a4bd00779faf6e2e2ebe23d441dfde8339

Naloga 6.4.3:
https://bitbucket.org/bertoncelj1/stroboskop/commits/d119771c58f4f76e302d092ca37eb215190d69a2

Naloga 6.4.4:
https://bitbucket.org/bertoncelj1/stroboskop/commits/9fdeb622f8ec14f382703f2f6d3b1269ed3cac63